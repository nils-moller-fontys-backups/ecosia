using System.Collections.Generic;

namespace FSM
{
    public class FiniteStateMachine
    {
        private readonly Stack<FsmState> stateStack;

        public delegate void FsmState(FiniteStateMachine fsm);

        public FiniteStateMachine()
        {
            this.stateStack = new Stack<FsmState>();
        }

        public void Invoke() // is update
        {
            this.stateStack.Peek()?.Invoke(this);
        }

        public void Push(FsmState state) => this.stateStack.Push(state);

        public void Pop() => this.stateStack.Pop();
    }
}
