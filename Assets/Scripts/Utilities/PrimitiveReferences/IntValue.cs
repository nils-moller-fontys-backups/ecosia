using UnityEngine;

namespace Utilities.PrimitiveReferences
{
    [CreateAssetMenu(fileName = "IntValue", menuName = "Primitives/Int", order = 1)]
    public class IntValue : ScriptableObject
    {
        [SerializeField] private int value = 0;

        public static implicit operator int(IntValue intValue) => intValue.value;
    }
}
