using UnityEngine;

namespace Utilities.PrimitiveReferences
{
    [CreateAssetMenu(fileName = "FloatValue", menuName = "Primitives/Float", order = 0)]
    public class FloatValue : ScriptableObject
    {
        [SerializeField] private float value = 0f;

        public static implicit operator float(FloatValue floatValue) => floatValue.value;
    }

}
