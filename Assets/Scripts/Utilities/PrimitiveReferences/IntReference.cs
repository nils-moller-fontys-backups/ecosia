using System;

using Sirenix.OdinInspector;

using UnityEngine;

namespace Utilities.PrimitiveReferences
{
    [Serializable]
    [InlineProperty]
    public class IntReference
    {
        [SerializeField]
        [HideLabel]
        [HorizontalGroup(5f)]
        /*[ValueDropdown(
            nameof(FloatReference.GetDropdownValues),
            AppendNextDrawer = true,
            DropdownWidth = 130)]*/
        private bool overrideValue = false;

        [SerializeField]
        [HideLabel]
        [HorizontalGroup]
        [HideIf(nameof(IntReference.overrideValue))]
        private IntValue intValue = null;

        [SerializeField]
        [HideLabel]
        [HorizontalGroup]
        [ShowIf(nameof(IntReference.overrideValue))]
        private int overridenValue = 0;

        /*private static IEnumerable GetDropdownValues() => new ValueDropdownList<bool>
        {
            { "Reference value", false },
            { "Overriden value", true },
        };*/

        public static implicit operator int(IntReference intRef) => intRef.overrideValue ? intRef.overridenValue : intRef.intValue;
    }
}
