using System;

using UnityEngine;
using UnityEngine.AI;

using Random = UnityEngine.Random;

namespace AI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(NavMeshAgent))]
    public class NavmeshMovement : MonoBehaviour
    {
        private NavMeshAgent navmeshAgent;

        private void Awake()
        {
            this.navmeshAgent = this.GetComponent<NavMeshAgent>();
        }

        public void SetDestination(Vector3 destination)
        {
            if (this.navmeshAgent.destination != destination)
                this.navmeshAgent.SetDestination(destination);
        }

        public Vector3 GetRandomDestination(float maxRange)
        {
            bool foundPath = false;
            NavMeshHit navMeshHit;
            do
            {
                bool foundPosition = NavMesh.SamplePosition(
                    this.transform.position + Random.insideUnitSphere * maxRange,
                    out navMeshHit,
                    maxRange,
                    NavMesh.AllAreas
                );

                if (!foundPosition)
                    continue;

                NavMeshPath path = new NavMeshPath();
                this.navmeshAgent.CalculatePath(navMeshHit.position, path);
                foundPath = path.status == NavMeshPathStatus.PathComplete;
            } while (!foundPath);

            return navMeshHit.position;
        }
    }
}
