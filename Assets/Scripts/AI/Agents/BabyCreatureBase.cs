using System.Collections.Generic;

using UnityEngine;

namespace AI.Agents
{
    [RequireComponent(typeof(GrowingBaby))]
    public abstract class BabyCreatureBase : CreatureBase
    {
        private UniqueDictionary<Condition, bool> currentGoal;

        public override UniqueDictionary<Condition, bool> CurrentGoal { get => this.currentGoal; }
        
        // todo: make it so we can return multiple goals and pick the one with lowest cost
        public override UniqueDictionary<Condition, bool> GetGoalState()
        {
            UniqueDictionary<Condition, bool> goal = new UniqueDictionary<Condition, bool>
            {
                { Condition.IsIdle, true },
            };

            this.currentGoal = goal;

            return goal;
        }
    }

}
