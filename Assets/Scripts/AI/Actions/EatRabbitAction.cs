using System.Collections.Generic;

using AI.Agents;

using UnityEngine;

namespace AI.Actions
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Hunger), typeof(Vision))]
    public class EatRabbitAction : TimedActionBase
    {
        private Hunger hunger;
        private Vision vision;
        private bool hasEaten = false;
        private Rabbit nearestFood = null;

        public override bool IsDone { get => this.hasEaten; }
        public override bool RequiresInRange { get => true; }
        public override float Cost { get => this.hunger.CurrentHunger / this.hunger.HungerThreshold * base.Cost; }

        protected override void Awake()
        {
            base.Awake();
            this.hunger = this.GetComponent<Hunger>();
            this.vision = this.GetComponent<Vision>();

            this.AddPrecondition(Condition.IsHungry, true);

            this.AddPostcondition(Condition.IsHungry, false);
        }

        protected override void Update()
        {
            base.Update();
            if (this.nearestFood != null)
                this.Target = this.nearestFood.transform.position;
        }

        protected override void OnReset()
        {
            base.OnReset();

            this.hasEaten = false;
            this.nearestFood = null;
        }

        public override bool CheckProceduralPreconditions()
        {
            List<Rabbit> possibleFoods = new List<Rabbit>();
            foreach (GameObject seenObject in this.vision.See())
            {
                if (seenObject.TryGetComponent(out Rabbit seenRabbit))
                    possibleFoods.Add(seenRabbit);
            }

            float distanceToNearestFood = float.MaxValue;

            foreach (Rabbit foodCheck in possibleFoods)
            {
                float distance = Vector3.Distance(foodCheck.transform.position, this.transform.position);

                if (distanceToNearestFood <= distance)
                    continue;

                distanceToNearestFood = distance;
                this.nearestFood = foodCheck;
            }

            return this.nearestFood != null;
        }

        public override bool Perform()
        {
            try
            {
                if (!base.Perform()) // timer not done yet
                    return true;

                this.hunger.Eat(this.nearestFood.gameObject);
                this.hasEaten = true;
                return true;
            }
            catch (MissingReferenceException)
            {
                return false;
            }
        }
    }

}
