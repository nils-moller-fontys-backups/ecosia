using UnityEngine;

namespace AI.Actions
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(PredatorAvoidance))]
    public class AvoidFoxAction : ActionBase
    {
        private PredatorAvoidance predatorAvoidance;
        private bool avoidedPredators;

        public override bool IsDone { get => this.avoidedPredators; }
        public override bool RequiresInRange { get => true; }

        protected override void Awake()
        {
            base.Awake();
            this.predatorAvoidance = this.GetComponent<PredatorAvoidance>();

            this.AddPrecondition(Condition.WantsToAvoidPredator, true);

            this.AddPostcondition(Condition.WantsToAvoidPredator, false);
        }

        private void Update()
        {
            if (this.predatorAvoidance.SeesPredator)
                this.Target = this.predatorAvoidance.AwayFromPredatorsTarget;
        }

        protected override void OnReset()
        {
            this.avoidedPredators = false;
        }

        public override bool CheckProceduralPreconditions() => this.predatorAvoidance.SeesPredator;

        public override bool Perform()
        {
            if (!this.predatorAvoidance.SeesPredator)
                this.avoidedPredators = true;

            return true;
        }
    }
}
