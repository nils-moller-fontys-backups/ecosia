using System;

using Sirenix.OdinInspector;

using UnityEngine;

using Utilities.PrimitiveReferences;

using Object = UnityEngine.Object;

namespace AI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Death))]
    public class Hunger : MonoBehaviour
    {
        private Death death;

        [SerializeField]
        private FloatReference hungerThreshold;

        [SerializeField]
        private FloatReference timeHungryUntilDeath;

        [SerializeField]
        [ReadOnly]
        [ProgressBar(0f, nameof(Hunger.hungerThreshold))]
        private float currentHunger = 0f;

        [SerializeField]
        [ReadOnly]
        [ProgressBar(0f, nameof(Hunger.timeHungryUntilDeath))]
        private float currentStarvingDuration = 0f;

        public bool WantsToEat { get => this.currentHunger >= this.hungerThreshold; }
        public float CurrentHunger { get => this.currentHunger; }
        public float HungerThreshold { get => this.hungerThreshold; }

        private void Awake()
        {
            this.death = this.GetComponent<Death>();
        }

        private void Update()
        {
            if (this.currentHunger < this.hungerThreshold)
                this.currentHunger += Time.deltaTime;
            else
                this.currentStarvingDuration += Time.deltaTime;

            if (this.currentStarvingDuration > this.timeHungryUntilDeath)
                this.death.Die();
        }

        public void Eat(GameObject foodToEat = null)
        {
            if (foodToEat != null)
                foodToEat.GetComponent<Death>().Die("Eaten by " + this.gameObject.name);

            this.currentHunger = 0f;
            this.currentStarvingDuration = 0f;
        }
    }
}
