using System.Collections.Generic;
using System.Linq;

using AI.Agents;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace AI
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Vision))]
    public class PredatorAvoidance : MonoBehaviour
    {
        private Vision vision;

        private List<Fox> seenPredators;

        public bool SeesPredator
        {
            get
            {
                if (this.seenPredators == null)
                    this.GetVisiblePredators();
                return this.seenPredators.Count > 0;
            }
        }

        public Vector3 AwayFromPredatorsTarget
        {
            get
            {
                if (this.seenPredators == null)
                    this.GetVisiblePredators();

                Vector3 direction = this.seenPredators.Aggregate(Vector3.zero, (current, other) => current + (this.transform.position - other.transform.position)) / this.seenPredators.Count;
                return this.transform.position + direction.normalized * 2f; // 2f just so the target isnt too close so it thinks it is there already
            }
        }

        private void Awake()
        {
            this.vision = this.GetComponent<Vision>();
        }

        private void LateUpdate()
        {
            this.seenPredators = null;
        }

        private void GetVisiblePredators()
        {
            this.seenPredators = new List<Fox>();
            foreach (GameObject seenObject in this.vision.See())
            {
                if (seenObject.TryGetComponent(out Fox seenPredator)) // todo: get type from inspector
                    this.seenPredators.Add(seenPredator);
            }
        }
    }
}
