using System;

using Sirenix.OdinInspector;

using UnityEngine;

using Utilities.PrimitiveReferences;

using Object = UnityEngine.Object;

namespace AI
{
    [DisallowMultipleComponent]
    public class Reproduction : MonoBehaviour
    {
        [SerializeField]
        private FloatReference reproductionDesireThreshold;

        [SerializeField]
        [ReadOnly]
        [ProgressBar(0f, nameof(Reproduction.reproductionDesireThreshold))]
        private float currentDesire = 0f;

        [SerializeField]
        private GameObject babyVersion;

        public bool WantsToReproduce { get => this.currentDesire >= this.reproductionDesireThreshold; }
        public Reproduction Partner { get; set; }
        public float CurrentDesire { get => this.currentDesire; }
        public float DesireThreshold { get => this.reproductionDesireThreshold; }

        private void Update()
        {
            if (this.currentDesire < this.reproductionDesireThreshold)
                this.currentDesire += Time.deltaTime;
        }

        public void Reproduce(Reproduction mate)
        {
            if (!this.WantsToReproduce)
                throw new InvalidOperationException("Not ready to reproduce yet.");

            Transform transform1 = this.transform;
            Object.Instantiate(
                this.babyVersion,
                transform1.position,
                transform1.rotation,
                transform1.parent
            );

            this.Partner = null;
            this.currentDesire = 0f;
            mate.currentDesire = 0f;
        }
    }
}
