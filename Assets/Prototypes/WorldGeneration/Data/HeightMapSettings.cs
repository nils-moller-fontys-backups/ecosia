using UnityEngine;

namespace Prototypes.WorldGeneration.Data
{
    [CreateAssetMenu(menuName = "Terrain Generation/Height Map Settings")]
    public class HeightMapSettings : UpdatableData
    {
        public NoiseSettings noiseSettings;

        public bool useFalloff;

        public float heightMultiplier = 50f;
        public AnimationCurve heightCurve;

        public float MinHeight { get => this.heightMultiplier * this.heightCurve.Evaluate(0); }
        public float MaxHeight { get => this.heightMultiplier * this.heightCurve.Evaluate(1); }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            this.noiseSettings.ValidateValues();

            base.OnValidate();
        }
#endif
    }
}
