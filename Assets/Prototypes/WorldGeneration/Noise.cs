using UnityEngine;

using Random = System.Random;

namespace Prototypes.WorldGeneration {
    public static class Noise
    {
        public enum NormalizeMode { Local = 0, Global = 1 }

        public static float[,] GenerateNoiseMap(int mapWidth,
                                                int mapHeight,
                                                NoiseSettings settings,
                                                Vector2 sampleCenter)
        {
            float[,] noiseMap = new float[mapWidth, mapHeight];

            Random prng = new Random(settings.seed);
            Vector2[] octaveOffsets = new Vector2[settings.octaves];

            float maxPossibleHeight = 0;
            float amplitude = 1f;

            for (int i = 0; i < settings.octaves; i++)
            {
                float offsetX = prng.Next(-100000, 100000) + settings.offset.x + sampleCenter.x;
                float offsetY = prng.Next(-100000, 100000) - settings.offset.y - sampleCenter.y;
                octaveOffsets[i] = new Vector2(offsetX, offsetY);

                maxPossibleHeight += amplitude;
                amplitude *= settings.persistence;
            }

            float maxLocalNoiseHeight = float.MinValue;
            float minLocalNoiseHeight = float.MaxValue;

            float halfWidth = mapWidth / 2f;
            float halfHeight = mapHeight / 2f;

            for (int y = 0; y < mapHeight; y++)
            {
                for (int x = 0; x < mapWidth; x++)
                {
                    amplitude = 1f;
                    float frequency = 1f;
                    float noiseHeight = 0;

                    for (int i = 0; i < settings.octaves; i++)
                    {
                        float sampleX = (x - halfWidth + octaveOffsets[i].x) / settings.scale * frequency;
                        float sampleY = (y - halfHeight + octaveOffsets[i].y) / settings.scale * frequency;

                        float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                        noiseHeight += perlinValue * amplitude;

                        amplitude *= settings.persistence;
                        frequency *= settings.lacunarity;
                    }

                    if (noiseHeight > maxLocalNoiseHeight)
                        maxLocalNoiseHeight = noiseHeight;

                    if (noiseHeight < minLocalNoiseHeight)
                        minLocalNoiseHeight = noiseHeight;

                    noiseMap[x, y] = noiseHeight;
                    if (settings.normalizeMode == NormalizeMode.Global)
                        noiseMap[x, y] = Mathf.Clamp((noiseMap[x, y] + 1) / maxPossibleHeight, 0, int.MaxValue);
                }
            }

            if (settings.normalizeMode == NormalizeMode.Local)
                for (int y = 0; y < mapHeight; y++)
                {
                    for (int x = 0; x < mapWidth; x++)
                        noiseMap[x, y] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[x, y]);
                }

            return noiseMap;
        }
    }

    [System.Serializable]
    public class NoiseSettings
    {
        public Noise.NormalizeMode normalizeMode = Noise.NormalizeMode.Global;

        public float scale = 50f;

        public int octaves = 6;
        [Range(0, 1)] public float persistence = 0.6f;
        public float lacunarity = 2f;

        public int seed;
        public Vector2 offset;

        public void ValidateValues()
        {
            this.scale = Mathf.Max(this.scale, 0.01f);
            this.octaves = Mathf.Max(this.octaves, 1);
            this.lacunarity = Mathf.Max(this.lacunarity, 1);
            this.persistence = Mathf.Clamp01(this.persistence);
        }
    }
}