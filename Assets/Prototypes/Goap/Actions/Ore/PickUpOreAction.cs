using AI;
using AI.Actions;

using Prototypes.Goap.Data;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace Prototypes.Goap.Actions.Ore
{
    [DisallowMultipleComponent]
    public class PickUpOreAction : ActionBase
    {

        [SerializeField] private FloatReference workerInteractionRange = null;
        [SerializeField] private int amountToPickup = 1;
        private bool hasOre;
        private SupplyPoint[] supplyPoints;
        private SupplyPoint targetSupplyPoint;

        public override bool RequiresInRange { get => true; }

        public override bool IsInRange
        {
            get
            {
                if (!this.RequiresInRange)
                    return true;

                return Vector3.Distance(this.transform.position, this.targetSupplyPoint.transform.position) < this.workerInteractionRange;
            }
        }

        public override bool IsDone { get => this.hasOre; }

        private new void Awake()
        {
            base.Awake();
            this.supplyPoints = Object.FindObjectsOfType<SupplyPoint>();

            this.hasOre = false;
            this.targetSupplyPoint = null;

            this.AddPrecondition(Condition.HasOre, false);
            this.AddPostcondition(Condition.HasOre, true);
        }

        protected override void OnReset()
        {
            this.hasOre = false;
            this.targetSupplyPoint = null;
        }

        public override bool CheckProceduralPreconditions()
        {
            SupplyPoint nearestSupplyPoint = null;
            float distanceToNearestSupplyPoint = float.MaxValue;

            foreach (SupplyPoint supplyPointCheck in this.supplyPoints)
            {
                if (supplyPointCheck.AmountOfOre < this.amountToPickup)
                    continue;

                float distance = Vector3.Distance(supplyPointCheck.gameObject.transform.position, this.gameObject.transform.position);

                if (distanceToNearestSupplyPoint <= distance)
                    continue;

                distanceToNearestSupplyPoint = distance;
                nearestSupplyPoint = supplyPointCheck;
            }

            if (nearestSupplyPoint == null)
                return false;

            this.targetSupplyPoint = nearestSupplyPoint;
            this.Target = nearestSupplyPoint.transform.position;

            return true;
        }

        public override bool Perform()
        {
            this.targetSupplyPoint.AmountOfOre -= this.amountToPickup;
            this.GetComponent<WorkerBackpack>().AmountOfOre += this.amountToPickup;
            this.hasOre = true;

            return true;
        }
    }
}
