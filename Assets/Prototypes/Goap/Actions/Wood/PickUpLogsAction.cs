using AI;
using AI.Actions;

using Prototypes.Goap.Data;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace Prototypes.Goap.Actions.Wood
{
    [DisallowMultipleComponent]
    public class PickUpLogsAction : ActionBase
    {

        [SerializeField] private int amountToPickup = 1;
        [SerializeField] private FloatReference workerInteractionRange = null;
        private bool hasLogs;
        private SupplyPoint[] supplyPoints;
        private SupplyPoint targetSupplyPoint;

        public override bool RequiresInRange { get => true; }

        public override bool IsInRange
        {
            get
            {
                if (!this.RequiresInRange)
                    return true;

                return Vector3.Distance(this.transform.position, this.targetSupplyPoint.transform.position) < this.workerInteractionRange;
            }
        }

        public override bool IsDone { get => this.hasLogs; }

        private new void Awake()
        {
            base.Awake();

            this.hasLogs = false;
            this.targetSupplyPoint = null;

            this.AddPrecondition(Condition.HasLogs, false);
            this.AddPostcondition(Condition.HasLogs, true);
        }

        protected override void OnReset()
        {
            this.hasLogs = false;
            this.targetSupplyPoint = null;
        }

        public override bool CheckProceduralPreconditions()
        {
            this.supplyPoints = Object.FindObjectsOfType<SupplyPoint>();
            SupplyPoint nearestSupplyPoint = null;
            float distanceToNearestSupplyPoint = float.MaxValue;

            foreach (SupplyPoint supplyPointCheck in this.supplyPoints)
            {
                if (supplyPointCheck.AmountOfLogs < this.amountToPickup)
                    continue;

                float distance = Vector3.Distance(supplyPointCheck.gameObject.transform.position, this.gameObject.transform.position);
                if (distanceToNearestSupplyPoint > distance)
                {
                    distanceToNearestSupplyPoint = distance;
                    nearestSupplyPoint = supplyPointCheck;
                }
            }

            if (nearestSupplyPoint == null)
                return false;

            this.targetSupplyPoint = nearestSupplyPoint;
            this.Target = nearestSupplyPoint.transform.position;

            return true;
        }

        public override bool Perform()
        {
            this.targetSupplyPoint.AmountOfLogs -= this.amountToPickup;
            this.GetComponent<WorkerBackpack>().AmountOfLogs += this.amountToPickup;
            this.hasLogs = true;

            return true;
        }
    }
}
