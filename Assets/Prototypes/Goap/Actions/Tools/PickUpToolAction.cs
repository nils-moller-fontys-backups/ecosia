using AI;
using AI.Actions;

using Prototypes.Goap.Data;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace Prototypes.Goap.Actions.Tools
{
    [DisallowMultipleComponent]
    public class PickUpToolAction : ActionBase
    {

        [SerializeField] private FloatReference workerInteractionRange = null;
        private bool hasTool;
        private SupplyPoint[] supplyPoints;
        private SupplyPoint targetSupplyPoint;

        public override bool RequiresInRange { get => true; }

        public override bool IsInRange
        {
            get
            {
                if (!this.RequiresInRange)
                    return true;

                return Vector3.Distance(this.transform.position, this.targetSupplyPoint.transform.position) < this.workerInteractionRange;
            }
        }

        public override bool IsDone { get => this.hasTool; }

        private new void Awake()
        {
            base.Awake();

            this.supplyPoints = Object.FindObjectsOfType<SupplyPoint>();

            this.hasTool = false;
            this.targetSupplyPoint = null;

            this.AddPrecondition(Condition.HasTool, false);
            this.AddPostcondition(Condition.HasTool, true);
        }

        protected override void OnReset()
        {
            this.hasTool = false;
            this.targetSupplyPoint = null;
        }

        public override bool CheckProceduralPreconditions()
        {
            SupplyPoint nearestSupplyPoint = null;
            float distanceToNearestSupplyPoint = float.MaxValue;

            foreach (SupplyPoint supplyPointCheck in this.supplyPoints)
            {
                if (supplyPointCheck.AmountOfTools < 1)
                    continue;

                float distance = Vector3.Distance(supplyPointCheck.gameObject.transform.position, this.gameObject.transform.position);
                if (distanceToNearestSupplyPoint > distance)
                {
                    distanceToNearestSupplyPoint = distance;
                    nearestSupplyPoint = supplyPointCheck;
                }
            }

            if (nearestSupplyPoint == null)
                return false;

            this.targetSupplyPoint = nearestSupplyPoint;
            this.Target = nearestSupplyPoint.transform.position;

            return true;
        }

        public override bool Perform()
        {
            this.targetSupplyPoint.AmountOfTools -= 1;
            this.GetComponent<WorkerBackpack>().AddTool();
            this.hasTool = true;

            return true;
        }
    }
}
