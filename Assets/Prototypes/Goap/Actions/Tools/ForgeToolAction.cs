using AI;
using AI.Actions;

using Prototypes.Goap.Data;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace Prototypes.Goap.Actions.Tools
{
    [DisallowMultipleComponent]
    public class ForgeToolAction : TimedActionBase
    {

        [SerializeField] private FloatReference workerInteractionRange = null;
        [SerializeField] private int amountOfOreNeeded = 3;
        [SerializeField] private int amountOfLogsNeeded = 3;
        private bool finishedForgingTool;
        private Forge[] forges;
        private Forge targetForge;

        public override bool RequiresInRange { get => true; }

        public override bool IsInRange
        {
            get
            {
                if (!this.RequiresInRange)
                    return true;

                return Vector3.Distance(this.transform.position, this.targetForge.transform.position) < this.workerInteractionRange;
            }
        }

        public override bool IsDone { get => this.finishedForgingTool; }

        protected override void Awake()
        {
            base.Awake();

            this.forges = Object.FindObjectsOfType<Forge>();

            this.targetForge = null;
            this.finishedForgingTool = false;

            this.AddPrecondition(Condition.HasOre, true);
            this.AddPrecondition(Condition.HasLogs, true);
            this.AddPrecondition(Condition.HasTool, false);

            this.AddPostcondition(Condition.HasOre, false);
            this.AddPostcondition(Condition.HasLogs, false);
            this.AddPostcondition(Condition.HasTool, true);
        }

        protected override void OnReset()
        {
            base.OnReset();
            this.targetForge = null;
            this.finishedForgingTool = false;
        }

        public override bool CheckProceduralPreconditions()
        {
            Forge nearestForge = null;
            float distanceToNearestForge = float.MaxValue;

            foreach (Forge forgeCheck in this.forges)
            {
                float distance = Vector3.Distance(forgeCheck.gameObject.transform.position, this.gameObject.transform.position);
                if (distanceToNearestForge > distance)
                {
                    distanceToNearestForge = distance;
                    nearestForge = forgeCheck;
                }
            }

            if (nearestForge == null)
                return false;

            this.targetForge = nearestForge;
            this.Target = nearestForge.transform.position;

            return true;
        }

        public override bool Perform()
        {
            WorkerBackpack backpack = this.GetComponent<WorkerBackpack>();
            if (backpack.AmountOfLogs < this.amountOfLogsNeeded || backpack.AmountOfOre < this.amountOfOreNeeded)
                return false;

            if (!base.Perform()) // if the timer hasnt finished
                return true;

            backpack.AmountOfLogs -= this.amountOfLogsNeeded;
            backpack.AmountOfOre -= this.amountOfOreNeeded;
            backpack.AddTool();
            this.finishedForgingTool = true;

            return true;
        }
    }
}
