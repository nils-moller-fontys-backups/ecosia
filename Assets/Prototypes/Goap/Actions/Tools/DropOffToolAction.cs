using AI;
using AI.Actions;

using Prototypes.Goap.Data;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace Prototypes.Goap.Actions.Tools
{
    [DisallowMultipleComponent]
    public class DropOffToolAction : ActionBase
    {

        [SerializeField] private FloatReference workerInteractionRange = null;
        private bool droppedOffTool;
        private SupplyPoint[] supplyPoints;
        private SupplyPoint targetSupplyPoint;

        public override bool IsDone { get => this.droppedOffTool; }
        public override bool RequiresInRange { get => true; }

        public override bool IsInRange
        {
            get
            {
                if (!this.RequiresInRange)
                    return true;

                return Vector3.Distance(this.transform.position, this.targetSupplyPoint.transform.position) < this.workerInteractionRange;
            }
        }

        protected override void Awake()
        {
            base.Awake();

            this.supplyPoints = Object.FindObjectsOfType<SupplyPoint>();

            this.droppedOffTool = false;
            this.targetSupplyPoint = null;

            this.AddPrecondition(Condition.HasTool, true);
            this.AddPostcondition(Condition.HasTool, false);
            this.AddPostcondition(Condition.CollectTools, true); // this is basically what goal this fulfills
        }

        protected override void OnReset()
        {
            this.droppedOffTool = false;
            this.targetSupplyPoint = null;
        }

        public override bool CheckProceduralPreconditions()
        {
            SupplyPoint nearestSupplyPoint = null;
            float distanceToNearestSupplyPoint = float.MaxValue;

            foreach (SupplyPoint supplyPointCheck in this.supplyPoints)
            {
                float distance = Vector3.Distance(supplyPointCheck.gameObject.transform.position, this.gameObject.transform.position);
                if (distanceToNearestSupplyPoint > distance)
                {
                    distanceToNearestSupplyPoint = distance;
                    nearestSupplyPoint = supplyPointCheck;
                }
            }

            if (nearestSupplyPoint == null)
                return false;

            this.targetSupplyPoint = nearestSupplyPoint;
            this.Target = nearestSupplyPoint.transform.position;

            return true;
        }

        public override bool Perform()
        {
            WorkerBackpack backpack = this.GetComponent<WorkerBackpack>();
            backpack.DropTool();
            this.targetSupplyPoint.AmountOfTools += 1;
            this.droppedOffTool = true;
            return true;
        }
    }
}
