using UnityEngine;

namespace Prototypes.Goap.Data
{
    [DisallowMultipleComponent]
    public class WorkerBackpack : MonoBehaviour
    {
        private Tool CurrentTool { get; set; }

        public int AmountOfOre { get; set; }
        public int AmountOfLogs { get; set; }
        public bool HasTool { get => this.CurrentTool != null; }

        private void Awake()
        {
            this.CurrentTool = new Tool(10); // have a tool when we start to enable gathering of resources
        }

        public void AddTool()
        {
            this.CurrentTool = new Tool(10);
        }

        public void DropTool()
        {
            this.CurrentTool = null;
        }

        public void ReduceToolDurability(int amount)
        {
            this.CurrentTool.Durability -= amount;

            if (this.CurrentTool.Durability > 0)
                return;

            this.CurrentTool = null;
        }
    }
}
