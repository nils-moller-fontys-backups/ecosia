using UnityEngine;

using Utilities.PrimitiveReferences;

namespace Prototypes.Goap.Data
{
    [DisallowMultipleComponent]
    public abstract class Harvestable : MonoBehaviour
    {

        [SerializeField] private FloatReference shrinkFactor = null;
        [SerializeField] private FloatReference regrowFactor = null;
        private Vector3 initialScale;
        public bool IsGrown { get; private set; }

        public bool IsClaimed { get; set; }

        private void Awake()
        {
            this.IsClaimed = false;
            this.IsGrown = true;
            this.initialScale = this.gameObject.transform.localScale;
        }

        private void Update()
        {
            if (this.gameObject.transform.localScale.magnitude < this.initialScale.magnitude)
            {
                this.gameObject.transform.localScale += this.initialScale * Time.deltaTime / this.regrowFactor;
                Transform transform1 = this.transform;
                Vector3 position = transform1.position;
                this.gameObject.transform.position = new Vector3(position.x, transform1.localScale.y / 2f, position.z);
            }
            else if (this.gameObject.transform.localScale.magnitude >= this.initialScale.magnitude && !this.IsGrown)
            {
                this.IsGrown = true;
            }
        }

        public virtual void Harvest()
        {
            this.IsGrown = false;
            this.IsClaimed = false;
            this.gameObject.transform.localScale /= this.shrinkFactor;
            Transform transform1 = this.transform;
            Vector3 position = transform1.position;
            this.gameObject.transform.position = new Vector3(position.x, transform1.localScale.y / 2f, position.z);
        }
    }
}
