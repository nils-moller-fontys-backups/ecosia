using System.Collections.Generic;

using AI;

using UnityEngine;

using Utilities.PrimitiveReferences;

namespace Prototypes.Goap.Agents
{
    [DisallowMultipleComponent]
    public class Blacksmith : WorkerBase
    {
        [SerializeField] private FloatReference amountOfOreNeededForTool = null;
        [SerializeField] private FloatReference amountOfLogsNeededForTool = null;

        public override UniqueDictionary<Condition, bool> GetCurrentState()
        {
            UniqueDictionary<Condition, bool> state = base.GetCurrentState();
            state[Condition.HasLogs] = this.Backpack.AmountOfLogs >= this.amountOfLogsNeededForTool;
            state[Condition.HasOre] = this.Backpack.AmountOfOre >= this.amountOfOreNeededForTool;
            return state;
        }

        /// <summary>
        ///     Creates tools from logs and ore.
        /// </summary>
        public override UniqueDictionary<Condition, bool> GetGoalState() => new UniqueDictionary<Condition, bool>
        {
            { Condition.CollectTools, true },
        };
    }
}
